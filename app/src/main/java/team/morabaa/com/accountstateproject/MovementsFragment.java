package team.morabaa.com.accountstateproject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import team.morabaa.com.accountstateproject.Model.Balance;
import team.morabaa.com.accountstateproject.Model.Company;
import team.morabaa.com.accountstateproject.Model.Movement;
import team.morabaa.com.accountstateproject.Utils.LocalData;
import team.morabaa.com.accountstateproject.Utils.SQLiteConnector;
import team.morabaa.com.accountstateproject.adapters.MovementsAdapter;

import static java.lang.System.out;
import static team.morabaa.com.accountstateproject.Utils.PublicClass.getAllChildren;

public class MovementsFragment extends Fragment {

    RequestQueue requestQueue;
    LinearLayout layoutMovementsContainer;
    private int userId;
    private int companyId;
    private int currencyId;
    private String companyName;
    private String currencyName;
    private SwipeRefreshLayout movementsSwipeRefresh;
    private String amount;
    private RecyclerView movementsRecyclerView;
    private Company currCompany;
    private ImageView IVCompanyPhoto;
    private int balanceId;
    private float FloatAmount;
    private float PrevAmount;

    public MovementsFragment() {
        // Required empty public constructor
        out.println("we in movments ^_^");
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalData.add(getContext(), "isFirst", "false");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        userId = getArguments().getInt("userId");
        companyId = getArguments().getInt("companyId");
        currCompany = Company.getCompanyById(getContext(), companyId);
        companyId = getArguments().getInt("companyId");
        balanceId = getArguments().getInt("balanceId");
        FloatAmount = getArguments().getFloat("FloatAmount");
        PrevAmount = getArguments().getFloat("PrevAmount");
        currencyId = getArguments().getInt("currencyId");
        companyName = getArguments().getString("companyName");
        currencyName = getArguments().getString("currencyName");
        amount = getArguments().getString("Amount");
        MainActivity.toolbar_back.setVisibility(View.VISIBLE);
        MainActivity.toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("userId", userId);
                MainFragment mainFragment = new MainFragment();
                mainFragment.setArguments(bundle);
                MovementsFragment.this.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.mainFrame, mainFragment).commit();
                out.println("back pressed ^_^");
            }
        });

        return inflater.inflate(R.layout.fragment_movements, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        layoutMovementsContainer = new LinearLayout(getContext()); //view.findViewById(R.id.movementsContainer);
        IVCompanyPhoto = (ImageView) view.findViewById(R.id.IVCompanyPhoto);
        movementsSwipeRefresh = view.findViewById(R.id.movementsSwipeRefresh);
        TextView txtCompanyName = view.findViewById(R.id.txtCompanyName);
        TextView txtAmount = view.findViewById(R.id.tXtAmount);
        TextView txtCurrency = view.findViewById(R.id.tXtCurrency);
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/CairoBold.ttf");
        requestQueue = Volley.newRequestQueue(getContext());
        movementsRecyclerView = view.findViewById(R.id.movementsRecyclerView);
        movementsSwipeRefresh.setRefreshing(true);
//scroll things
//        scrollViewSub.getViewTreeObserver().addOnScrollChangedListener(
//                new ViewTreeObserver.OnScrollChangedListener() {
//                    int prev = 0;
//
//                    @Override
//                    public void onScrollChanged() {
//                     //   Toast.makeText(getContext(), ""+scrollViewSub.getScrollY(), Toast.LENGTH_SHORT).show();
//                        if (scrollViewSub.getChildAt(0).getBottom() ==
//                                (scrollViewSub.getHeight() + scrollViewSub.getScrollY())) {
//                            bottomProgressBar.setVisibility(View.VISIBLE);
//                            new CountDownTimer(5000, 1) {
//
//                                @Override
//                                public void onTick(long millisUntilFinished) {
//                                    // do something after 1s
//                                }
//
//                                @Override
//                                public void onFinish() {
//                                    bottomProgressBar.setVisibility(View.GONE);
//                                }
//
//                            }.start();
//                        }
//
//                        int now = scrollViewSub.getScrollY();
//
//                        if (prev > now) {
//                            txtCompanyName.setVisibility(View.VISIBLE);
//                            scrollViewSub.setBackgroundColor(Color.BLUE);
//                        } else {
//                            scrollViewSub.setBackgroundColor(Color.RED);
//                            txtCompanyName.setVisibility(View.GONE);
//                        }
//                        prev = now;
//                    }
//                });
//
        if (currCompany.getPhoto_url() != null) {
            byte[] decodedString = Base64.decode(currCompany.getPhoto_url(), Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            IVCompanyPhoto.setImageBitmap(bmp);
        }
        txtCompanyName.setText(companyName);
        txtAmount.setText(amount);
        txtCurrency.setText(currencyName);
        txtCompanyName.setTypeface(font);

        movementsSwipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
//                        layoutMovementsContainer.removeAllViews();
                        StringRequest stringRequest = getMovements(
                                getContext(),
                                userId,
                                companyId,
                                currencyId,
                                currencyName,
                                movementsSwipeRefresh,
                                layoutMovementsContainer);
                        requestQueue.add(stringRequest);
                    }
                });

        PrevAmount= new SQLiteConnector(getContext()).GetPrevAmount(balanceId);
        if (FloatAmount == PrevAmount) {
            getMovementsOffline();
          //  Toast.makeText(getContext(), "offline", Toast.LENGTH_SHORT).show();
        } else {
            getMovementsOnline();
       //     Toast.makeText(getContext(), "online", Toast.LENGTH_SHORT).show();
          new SQLiteConnector(getContext()).updatePrevAmount(FloatAmount,balanceId);
        }
        for (View v : getAllChildren(view)) {
            handleBackPress(v);
        }
    }

    private void getMovementsOnline() {
        StringRequest stringRequest = getMovements(
                getContext(),
                userId,
                companyId,
                currencyId,
                currencyName,
                movementsSwipeRefresh,
                layoutMovementsContainer);
        requestQueue.add(stringRequest);
    }

    private void getMovementsOffline() {
        List<Movement> movements = Movement.getData(getContext(), userId, companyId, currencyId);
        if (movements.size() == 0) {
            getMovementsOnline();
        }
        movementsRecyclerView.setAdapter(
                new MovementsAdapter(
                        getContext(), movements, currencyName)
        );
        movementsRecyclerView.setLayoutManager(
                new LinearLayoutManager(getContext())
        );
        movementsSwipeRefresh.setRefreshing(false);
    }

    public StringRequest getMovements(Context ctx,
                                      int userId,
                                      int companyId,
                                      int currencyId,
                                      String currencyName,
                                      SwipeRefreshLayout movementsSwipeRefresh,
                                      LinearLayout layoutMovementsContainer) {


        String URL = PV.getMovementsURL();
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("UserID", userId);
            jsonBody.put("CompanyId", companyId);
            jsonBody.put("CurrencyId", currencyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String requestBody = jsonBody.toString();
        return new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        MainActivity.online_error_btn_refresh.setVisibility(View.GONE);
                        List<Movement> movements = new ArrayList<>();
                        try {
                            movements = Movement.parse(s, userId);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        /*Movement.showContent(movements,
                                movementsSwipeRefresh,
                                layoutMovementsContainer,
                                currencyName,
                                ctx);*/
                        movementsRecyclerView.setAdapter(
                                new MovementsAdapter(
                                        getContext(), movements, currencyName)
                        );
                        movementsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        movementsSwipeRefresh.setRefreshing(false);
                        Movement.delete(ctx,
                                userId,
                                companyId,
                                currencyId);
                        for (Movement movement : movements) {
                            Movement.add(ctx, movement);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.toString());
                        List<Movement> movements = Movement.getData(ctx, userId, companyId, currencyId);
                        Log.e("VOLLEY", "-------\tmovements size\t------------" + movements.size());
                        movementsRecyclerView.setAdapter(
                                new MovementsAdapter(
                                        getContext(), movements, currencyName)
                        );
                        movementsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        movementsSwipeRefresh.setRefreshing(false);

                        MainActivity.online_error_btn_refresh.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                movementsSwipeRefresh.setRefreshing(true);
                                StringRequest mo = getMovements(ctx,
                                        userId,
                                        companyId,
                                        currencyId,
                                        currencyName,
                                        movementsSwipeRefresh,
                                        layoutMovementsContainer);
                                Volley.newRequestQueue(ctx).add(mo);
                            }
                        });
                        MainActivity.online_error_btn_refresh.setVisibility(View.VISIBLE);
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {

                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
    }

    @Override
    public void onDetach() {
        super.onDetach();
        layoutMovementsContainer.removeAllViews();

    }


    private void handleBackPress(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(
                new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            Bundle bundle = new Bundle();
                            bundle.putInt("userId", userId);
                            MainFragment mainFragment = new MainFragment();
                            mainFragment.setArguments(bundle);
                            MovementsFragment.this.getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.mainFrame, mainFragment).commit();
                            out.println("back pressed ^_^");
                            return true;
                        }
                        return false;
                    }
                }
        );
    }
}