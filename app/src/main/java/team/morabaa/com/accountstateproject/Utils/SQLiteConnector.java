package team.morabaa.com.accountstateproject.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import team.morabaa.com.accountstateproject.Model.Movement;


public class SQLiteConnector extends SQLiteOpenHelper {

    private static final String dbName = "db";


    public SQLiteConnector(Context context) {
        super(context, dbName, null, 7);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String companiesTable =
                "CREATE TABLE `companies` (" +
                        "`id`INTEGER NOT NULL PRIMARY KEY UNIQUE," +
                        " `name` VARCHAR(512) ," +
                        " `lastUpdate` VARCHAR(512), " +
                        "`photoURL` BLOB);";
        db.execSQL(companiesTable);

        String currenciesTable =
                "CREATE TABLE `currencies` (" +
                        "`id`INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                        " `name` VARCHAR(512) ," +
                        " `symple` VARCHAR(512) );";
        db.execSQL(currenciesTable);


        db.execSQL(Movement.createTable);

        String balancesTable =
                "CREATE TABLE `balances` (" +
                        "`id`INTEGER NOT NULL PRIMARY KEY," +
                        "`userId`INTEGER NOT NULL," +
                        "`companyId`INTEGER NOT NULL," +
                        "`currencyId`INTEGER NOT NULL," +
                        "`amount`REAL NOT NULL," +
                        "`lastUpdate`VARCHAR(512)," +
                        "`PrevAmount`REAL" +
                        ");";
        db.execSQL(balancesTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists companies");
        db.execSQL("drop table if exists balances");
        db.execSQL("drop table if exists currencies");
        db.execSQL("drop table if exists movements");
        onCreate(db);
    }

    public void add(String tableName, ContentValues values) {
        System.out.println("data insertsd into " + tableName + " table successfully");
        getWritableDatabase().insert(tableName, "name", values);
    }

    public void delete(String tableName, String where, String[] args) {
        getWritableDatabase().delete(tableName, where, args);
    }

    public Cursor getData(String tableName) {
        String sql = "SELECT * FROM " + tableName;
        return getReadableDatabase().rawQuery(sql, null);
    }

    public void excute(String s) {
        getWritableDatabase().execSQL(s);
    }


    public void updateCompanyImage(byte[] decodedString, int companyId) {
        try {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("photoURL", decodedString);
//            database.update("companies", cv, "Id = " + companyId, null);
            database.update("companies",cv,"Id="+companyId,null);

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public float GetPrevAmount(int balanceId) {
        int p=0;
        float p2 = 0;
        String sql = "SELECT * FROM balances where id= "+balanceId;
        Cursor prevamount = getReadableDatabase().rawQuery(sql, null);
        while (prevamount.moveToNext()) {
            p = prevamount.getInt(6);
           float p1 = prevamount.getFloat(0);
            p2 = prevamount.getFloat(4);
            Log.v("previus", p2+" "+p + "  "+p1);
        }
        return p2;
    }

    public void update(String tableName, ContentValues values, String where) {
        getWritableDatabase().update(tableName, values, where, null);
    }

    public void updatePrevAmount(float floatAmount, int balanceId) {
        try {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("PrevAmount", floatAmount);
//            database.update("companies", cv, "Id = " + companyId, null);
            database.update("balances",cv,"Id="+balanceId,null);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
