package team.morabaa.com.accountstateproject.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import team.morabaa.com.accountstateproject.Model.Balance;
import team.morabaa.com.accountstateproject.Model.Movement;
import team.morabaa.com.accountstateproject.R;

import static team.morabaa.com.accountstateproject.Model.Movement.formattedDate;

/**
 * -Created by eagle on 11/2/2017.
 */

public class MovementsAdapter extends RecyclerView.Adapter<MovementsViewHolder> {
    private List<Movement> movements;
    private Context ctx;
    private String currencyName;

    public MovementsAdapter(Context ctx,
                            List<Movement> movements,
                            String currencyName) {
        this.movements = movements;
        this.ctx = ctx;
        this.currencyName = currencyName;
    }

    @Override
    public MovementsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.movement_layout, parent, false);
        return new MovementsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovementsViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Typeface fontBold = Typeface.createFromAsset(ctx.getAssets(), "fonts/CairoBold.ttf");
        Typeface font = Typeface.createFromAsset(ctx.getAssets(), "fonts/CairoRegular.ttf");

        holder.txtMovementType.setTypeface(fontBold);
        holder.txtMovementType.setText(Balance.BalanceTypeById(movements.get(position).getType()));

        holder.tXtCurrency.setTypeface(font);
        holder.tXtCurrency.setText(currencyName);

        holder.tXtAmount.setTypeface(font);
        holder.tXtAmount.setText(String.valueOf(movements.get(position).getStringAmount()));

        holder.txtMovementDescription.setTypeface(font);
        holder.txtMovementDescription.setText(movements.get(position).getDescription());

        if (movements.get(position).getAmount() < 0) {
            holder.imageView.setImageResource(R.drawable.as_green_arrow);

            holder.tXtAmount.setTextColor(Color.parseColor("#00E676"));
            //holder.tXtCurrency.setTextColor(Color.parseColor("#00E676"));
        }
        else{  holder.imageView.setImageResource(R.drawable.as_red_arrow);

            holder.tXtAmount.setTextColor(Color.parseColor("#ff8a80"));
           // holder.tXtCurrency.setTextColor(Color.RED);

        }


        holder.txtDate.setTypeface(font);
        String date = formattedDate(movements.get(position).getDate());
        holder.txtDate.setText(date);


    }

    @Override
    public int getItemCount() {
        return movements.size();
    }


}

class MovementsViewHolder extends RecyclerView.ViewHolder {
    LinearLayout view;
    TextView txtMovementType;
    TextView tXtCurrency;
    TextView tXtAmount;
    TextView txtMovementDescription;
    ImageView imageView;
    TextView txtDate;

    MovementsViewHolder(View view) {
        super(view);
        this.view = (LinearLayout) itemView;
        this.txtMovementType = view.findViewById(R.id.txtMovementType);
        this.tXtCurrency = view.findViewById(R.id.tXtCurrency);
        this.tXtAmount = view.findViewById(R.id.tXtAmount);
        this.txtMovementDescription = view.findViewById(R.id.txtMovementDescription);
        this.imageView = view.findViewById(R.id.imageViewCurrencyIcon);
        this.txtDate = view.findViewById(R.id.txtDate);
    }
}