package team.morabaa.com.accountstateproject.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import team.morabaa.com.accountstateproject.Utils.PublicClass;
import team.morabaa.com.accountstateproject.Utils.SQLiteConnector;

/**
 * Created by eagle on 9/19/2017.
 */

public class Balance {
    private static final String tableName = "balances";

    private int id;
    private int userId;
    private int companyId;
    private int currencyId;
    private float amount;
    private float PrevAmount;
    private String lastUpdate;


    public static void add(Context ctx, Balance balance) {

        ContentValues values = new ContentValues();
        values.put("id", balance.getId());
        values.put("userId", balance.getUserId());
        values.put("companyId", balance.getCompanyId());
        values.put("currencyId", balance.getCurrencyId());
        values.put("amount", balance.getAmount());
        values.put("lastUpdate", balance.getLastUpdate());
        values.put("PrevAmount", balance.getPrevAmount());

        new SQLiteConnector(ctx).add(tableName, values);
    }

    public static void update(Context ctx, Balance balance) {

        ContentValues values = new ContentValues();
        values.put("id", balance.getId());
        values.put("userId", balance.getUserId());
        values.put("companyId", balance.getCompanyId());
        values.put("currencyId", balance.getCurrencyId());
        values.put("amount", balance.getAmount());
        values.put("lastUpdate", balance.getLastUpdate());
        values.put("PrevAmount", balance.getAmount());

        String where =
                " userId=" + balance.getUserId() +
                        " companyId=" + balance.getCompanyId() +
                        " currencyId=" + balance.getCurrencyId();
        new SQLiteConnector(ctx).update(tableName, values, where);
    }



    public static void delete(Context ctx, int userId) {
        new SQLiteConnector(ctx).delete(tableName,
                "userId=? ",
                new String[]{String.valueOf(userId)});
    }

    public static Balance getSingleBalance(Context ctx, int userId, int companyId, int currencyId) {
        Cursor cursor = new SQLiteConnector(ctx).getData(tableName);
        Log.e("VOLLEY", "--------\tcursor Count\t-----------" + cursor.getCount());
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            if (
                    cursor.getInt(1) == userId &&
                            cursor.getInt(2) == companyId &&
                            cursor.getInt(3) == currencyId
                    ) {
                return new Balance() {{
                    setId(cursor.getInt(0));
                    setUserId(cursor.getInt(1));
                    setCompanyId(cursor.getInt(2));
                    setCurrencyId(cursor.getInt(3));
                    setAmount(cursor.getInt(4));
                    setLastUpdate(cursor.getString(5));
                    setPrevAmount(cursor.getInt(6));
                    System.out.println("aaa data "+cursor.getInt(6));
                }};
            }
            cursor.moveToNext();
        }
        return new Balance() {
            {
                setPrevAmount(0);
            }
        };
    }

    public static List<Balance> getData(Context ctx, int userId) {
        List<Balance> balances = new ArrayList<>();
        Cursor cursor = new SQLiteConnector(ctx).getData(tableName);
        Log.e("VOLLEY", "--------\tcursor Count\t-----------" + cursor.getCount());
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            if (cursor.getInt(1) == userId) {
                Log.e("VOLLEY", cursor.getInt(1) + "");
                balances.add(
                        new Balance() {{
                            setId(cursor.getInt(0));
                            setUserId(cursor.getInt(1));
                            setCompanyId(cursor.getInt(2));
                            setCurrencyId(cursor.getInt(3));
                            setAmount(cursor.getInt(4));
                            setLastUpdate(cursor.getString(5));
                        }}
                );
            }
            cursor.moveToNext();
        }
        Log.e("VOLLEY", "----------------\t" + balances.size());

        return balances;
    }

    public static String BalanceTypeById(int id) {
        switch (id) {
            case 0:
                return "سند قبض";
            case 1:
                return "سند دفع";
            case 2:
                return "قائمة بيع";
            case 3:
                return "سندات التحويل";
            case 4:
                return "تعديل رصيد حساب";
            case 5:
                return "سند صرف";
            case 6:
                return "سند تحويل حوالة";
            case 7:
                return "سند صيرفة";
            case 8:
                return "8";
            case 9:
                return "قائمة شراء";
            case 10:
                return "سند توزيع ارباح";
            case 11:
                return "ارجاع قائمة بيع";
            case 12:
                return "ارجاع قائمة شراء";
            case 13:
                return "ادخال مادة";
            case 14:
                return "التالف من المواد";
            case 15:
                return "التالف من المواد";
            case 16:
                return "لا ترصد";
            case 17:
                return "مصاريف المواد";
            case 18:
                return "عرض سعر";
            case 19:
                return "كلفة المواد";
            case 20:
                return "طلبية المواد";
            case 21:
                return "قيد تصفير ارباح والمصاريف";
            case 22:
                return "قيد محاسبي";
            case 23:
                return "قيد محاسبي معقد";
            case 24:
                return "حوالة بالصيرفة";
            case 25:
                return "لا ترصد خراعة خضره";
            case 26:
                return "شراء العملات";
            case 27:
                return "بيع العملات";
            case 28:
                return "لا ترصد خراعة خضره -_- للكل ولا يوصل قواعد بيانات";
            case 29:
                return "قائمة مطعم صالة";
            case 30:
                return "قائمة مطعم سفري";
            case 31:
                return "قائمة مطعم دلفري";
            case 32:
                return "عكس حوالة صيرفة";
            case 33:
                return "تحويل الى حساب اخر";
            case 34:
                return "سند مبادلة عملات";
        }
        return "";
    }


    //region get and set
    public String getStringAmount() {
        return PublicClass.ToFormattedPrice(amount);
    }


    public void setAmount(float amount) {
        this.amount = amount;
    }


    public static String getTableName() {
        return tableName;
    }


    public float getPrevAmount() {
        return PrevAmount;
    }

    public void setPrevAmount(float prevAmount) {
        PrevAmount = prevAmount;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    public float getAmount() {
        return amount;
    }


    //endregion
}

