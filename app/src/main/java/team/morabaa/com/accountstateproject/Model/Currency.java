package team.morabaa.com.accountstateproject.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import team.morabaa.com.accountstateproject.PV;
import team.morabaa.com.accountstateproject.Utils.SQLiteConnector;

import static team.morabaa.com.accountstateproject.MainFragment.refreshAdapter;

/**
 * Created by eagle on 9/19/2017.
 */

public class Currency {
    private static final String tableName = "currencies";
    private int id;
    private String name;
    private String symple;

    public Currency() {

    }


    public static String getSympleById(Context ctx, int id) {
        for (Currency currency : getData(ctx)) {
            if (currency.getId() == id) {
                return currency.getSymple();
            }
        }
        return "";
    }

    public static void add(Context ctx, Currency currency) {
        ContentValues values = new ContentValues();
        values.put("id", currency.getId());
        values.put("name", currency.getName());
        values.put("symple", currency.getSymple());
        boolean exist = false;
        for (Currency c : getData(ctx)) {
            if (c.getId() == currency.getId())
                exist = true;
        }
        if (!exist)
            new SQLiteConnector(ctx).add(tableName, values);
    }

    public static List<Currency> getData(Context ctx) {
        List<Currency> currencies = new ArrayList<>();
        Cursor cursor = new SQLiteConnector(ctx).getData(tableName);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            {
                currencies.add(
                        new Currency() {{
                            setId(cursor.getInt(0));
                            setName(cursor.getString(1));
                            setSymple(cursor.getString(2));
                        }}
                );
            }
            cursor.moveToNext();
        }
        return currencies;
    }

    public static void getCurrenciesNames(Context ctx, final int id,
                                          RecyclerView recyclerView) {
        List<Currency> currencies = new ArrayList<>();
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(ctx);
            String URL = PV.getCurrenciesURL();
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("MyUserID", id);
            final String requestBody = jsonBody.toString();
            StringRequest stringRequest =
                    new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray mainObject = new JSONArray(response);
                                for (int i = 0; i < mainObject.length(); i++) {
                                    JSONObject j = mainObject.getJSONObject(i);
                                    currencies.add(new Currency() {{
                                                       setId(j.getInt("Id"));
                                                       setSymple(j.getString("Symbol"));
                                                       setName(j.getString("Name"));
                                                   }}
                                    );
                                }
                                for (Currency currency : currencies) {
                                    Currency.add(ctx, currency);
                                }

                                refreshAdapter();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("VOLLEY", error.toString());
                        }
                    }) {
                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }

                        @Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response) {

                            String parsed;
                            try {
                                parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                            } catch (UnsupportedEncodingException e) {
                                parsed = new String(response.data);
                            }
                            return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
                        }
                    };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        return currencies;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymple() {
        return symple;
    }

    public void setSymple(String symple) {
        this.symple = symple;
    }


}
