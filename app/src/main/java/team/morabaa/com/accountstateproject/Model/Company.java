package team.morabaa.com.accountstateproject.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import team.morabaa.com.accountstateproject.PV;
import team.morabaa.com.accountstateproject.Utils.SQLiteConnector;

import static team.morabaa.com.accountstateproject.MainFragment.refreshAdapter;

/**
 * Created by eagle on 9/19/2017.
 */

public class Company {
    private static final String tableName = "companies";

    //region Properties
    private int id;
    private static int userId;
    private byte[] photo_url;
    private String name;
    private String lastUpdate;
    private List<Balance> balances;


//endregion

    public static List<Company> getData(List<Balance> balances) {
        List<Integer> companiesIdList = new ArrayList<>();
        for (Balance balance : balances) {
            if (!companiesIdList.contains(balance.getCompanyId()))
                companiesIdList.add(balance.getCompanyId());
        }

        List<Company> companies = new ArrayList<>();
        for (final Integer companyId : companiesIdList) {
            final List<Balance> balanceList = new ArrayList<>();
            String lastUpdate=""; // get max value ..
            for (Balance balance : balances) {
                if (companyId == balance.getCompanyId()) {
                    balanceList.add(balance);
                    lastUpdate=balance.getLastUpdate(); // compare ..
                }
            }
            String finalLastUpdate = lastUpdate;
            companies.add(
                    new Company() {{
                        setId(companyId);
                        setUserId(balances.get(0).getUserId());
                        setLastUpdate(finalLastUpdate);
//                        setLastUpdate(balances.get(0).getLastUpdate());
                        setBalances(balanceList);
                    }}
            );
        }
        return companies;
    }

    public static String companyNameById(Context ctx, int id) {
        if (getData(ctx) != null) {
            for (Company company : getData(ctx)) {
                if (company.getId() == id)
                    return company.getName();
            }
        }
        return "";
    }

    public static int getCompaniesCount(Context ctx) {
        return getData(ctx).size();
    }

    public static void getCompaniesNames(Context ctx, final int id, RecyclerView recyclerView) {
        List<Company> companies = new ArrayList<>();
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(ctx);
            String URL = PV.getCompaniesURL();
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("MyUserID", id);

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest =
                    new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject j = jsonArray.getJSONObject(i);
                                    companies.add(
                                            new Company() {{
                                                setId(j.getInt("Id"));
                                                setName(j.getString("Name"));
                                            }}
                                    );
                                }
                                for (Company company : companies) {
                                    add(ctx, company);
                                }

                                refreshAdapter();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                        Toast.makeText(getContext(), companies.size() + "", Toast.LENGTH_SHORT).show();

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("VOLLEY", error.toString());
                        }
                    }) {
                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }

                        @Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response) {

                            String parsed;
                            try {
                                parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                            } catch (UnsupportedEncodingException e) {
                                parsed = new String(response.data);
                            }
                            return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
                        }
                    };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        return companies;
    }


    public static void add(Context ctx, Company company) {

        ContentValues values = new ContentValues();
        values.put("id", company.getId());
        values.put("name", company.getName());

        boolean exist = false;
        for (Company c : getData(ctx)) {
            if (c.getId() == company.getId())
                exist = true;
        }
        if (!exist)
            new SQLiteConnector(ctx).add(tableName, values);
    }

    public static List<Company> getData(Context ctx) {
        List<Company> companies = new ArrayList<>();
        Cursor cursor = new SQLiteConnector(ctx).getData(tableName);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            {
                companies.add(
                        new Company() {{
                            setId(cursor.getInt(0));
                            setName(cursor.getString(1));
                            setLastUpdate(cursor.getString(2));
                            setPhoto_url(cursor.getBlob(3));
                            Log.v("companies",cursor.getInt(0)+cursor.getString(1)+cursor.getString(2)+cursor.getBlob(3));
                        }}
                );
            }
            cursor.moveToNext();
        }
        return companies;
    }

    public static Company getCompanyById(Context ctx, int id) {
        Cursor cursor = new SQLiteConnector(ctx).getData(tableName);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            {
                if (cursor.getInt(0) == id) {
                    return
                            new Company() {{
                                setId(cursor.getInt(0));
                                setName(cursor.getString(1));
                                setLastUpdate(cursor.getString(2));
                                setPhoto_url(cursor.getBlob(3));
                            }};
                }
            }
            cursor.moveToNext();
        }
        return null;
    }

    //region g and p
    public byte[] getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(byte[] photo_url) {
        this.photo_url = photo_url;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        Company.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Balance> getBalances() {
        return balances;
    }

    public void setBalances(List<Balance> balances) {
        this.balances = balances;
    }


    //endregion
    public static void  updateCompanyImage(Context ctx, byte[] decodedString, int companyId) {

        ContentValues cv = new ContentValues();
        cv.put("photoURL", decodedString);
        new SQLiteConnector(ctx).update(tableName, cv, "id=" + companyId);
    }

}
