package team.morabaa.com.accountstateproject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import team.morabaa.com.accountstateproject.Utils.LocalData;
import team.morabaa.com.accountstateproject.Utils.PublicClass;
import team.morabaa.com.accountstateproject.Utils.SQLiteConnector;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.content.ContentValues.TAG;
import static com.google.firebase.auth.FirebaseAuth.getInstance;

public class LoginActivity extends AppCompatActivity {

    String phoneNo;
    LinearLayout lMain;
    LinearLayout lConfirm;
    EditText txtConfirm;
    EditText txtPhoneNo;
    Button btn_comfirm;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private boolean mVerificationInProgress;
    private FirebaseAuth mAuth;
    private Dialog progress1;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private Button btn_signIn;
    private int userId;
    private LinearLayout above;
    private ImageView imgLogo;
    private static final int PERMISSION_READ_STATE = 502;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(PublicClass.fontCiro)
                .setFontAttrId(R.attr.fontPath)
                .build());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

//        Intent companiesIntent2 = new Intent(LoginActivity.this, MainActivity.class);
//        startActivity(companiesIntent2);
//        LocalData.add(getApplication(), "userId", );

        imgLogo = findViewById(R.id.imgLogo);
        above = findViewById(R.id.above);
        btn_signIn = findViewById(R.id.btn_signIn);
        btn_comfirm = findViewById(R.id.btn_comfirm);
        btn_signIn.setTypeface(PublicClass.BoldFont(this));
        btn_comfirm.setTypeface(PublicClass.BoldFont(this));
        String userId = LocalData.get(this, "userId");

        if (!userId.equals("")) {
//            progress1.dismiss();

            Intent companiesIntent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(companiesIntent);

        } else {

            mAuth = getInstance();
            // mAuth.setLanguageCode("iq");
            mAuth.useAppLanguage();

            int result = 0;
            for (int I = 0; I < 5; I++) {
                if (I == 3) {
                    result += 10;
                } else {
                    result += I;
                }
                System.out.println(I + "\t" + result);
            }
            System.out.println(result);

            new SQLiteConnector(LoginActivity.this);
            lMain = findViewById(R.id.main);
            lConfirm = findViewById(R.id.confirm);
            txtConfirm = findViewById(R.id.txtConfirm);
            txtPhoneNo = findViewById(R.id.txtPhoneNo);
            mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                @Override
                public void onVerificationCompleted(PhoneAuthCredential credential) {
                    // This callback will be invoked in two situations:
                    // 1 - Instant verification. In some cases the phone number can be instantly
                    //     verified without needing to send or enter a verification code.
                    // 2 - Auto-retrieval. On some devices Google Play services can automatically
                    //     detect the incoming verification SMS and perform verificaiton without
                    //     user action.
                    Log.d(TAG, "onVerificationCompleted:" + credential);

                    signInWithPhoneAuthCredential(credential);
                }

                @Override
                public void onVerificationFailed(FirebaseException e) {
                    // This callback is invoked in an invalid request for verification is made,
                    // for instance if the the phone number format is not valid.
                    Log.w(TAG, "onVerificationFailed", e);
                    progress1.dismiss();
                    Toast.makeText(getApplicationContext(), "يرجى ادخال رقم هاتف صالح", Toast.LENGTH_SHORT).show();

                    if (e instanceof FirebaseAuthInvalidCredentialsException) {
                        // Invalid request
                        // ...
                    } else if (e instanceof FirebaseTooManyRequestsException) {
                        // The SMS quota for the project has been exceeded
                        // ...
                    }

                    // Show a message and update the UI
                    // ...
                }

                @Override
                public void onCodeSent(String verificationId,
                                       PhoneAuthProvider.ForceResendingToken token) {
                    // The SMS verification code has been sent to the provided phone number, we
                    // now need to ask the user to enter the code and then construct a credential
                    // by combining the code with a verification ID.
                    Log.d(TAG, "onCodeSent:" + verificationId);
                    // Save verification ID and resending token so we can use them later
                    lMain.setVisibility(View.GONE);
                    lConfirm.setVisibility(View.VISIBLE);
//                    ImageView imgLogo = (ImageView) findViewById(R.id.imgLogo);
//                    imgLogo.setBackgroundResource(R.drawable.confirm_logo);
                    LocalData.add(getApplicationContext(), "verificationId", verificationId);
                    //                Toast.makeText(getApplicationContext(), "الية التاكيد verificationId", Toast.LENGTH_LONG).show();
                    progress1.dismiss();
                    mVerificationId = verificationId;
                    mResendToken = token;

                    // ...
                }
            };
        }

        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        //region hide/show logo on keyboar appear
        final View activityRootView = findViewById(R.id.activity_login);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();

                activityRootView.getWindowVisibleDisplayFrame(r);

                int heightDiff = above.getRootView().getHeight() - (r.bottom - r.top);
                if (heightDiff > 200) {
                    //enter your code here
                    imgLogo.setVisibility(View.GONE);


                } else {
                    imgLogo.setVisibility(View.VISIBLE);

                    //enter code for hid
                }
            }
        });
        //endregion


    }

    public void login(View view) {

        if ((txtPhoneNo.getText().toString().trim()).length() > 0) {
            progress1 = ProgressDialog.show(new ContextThemeWrapper(this, R.style.progressDialog), "تسجيل الدخول",
                    "يرجى الانتظار قليلاً ....", true);
            progress1.setCancelable(true);
            startPhoneNumberVerification(txtPhoneNo.getText().toString());
        } else {
            Toast.makeText(getBaseContext(), "يرجى ادخال رقم الهاتف", Toast.LENGTH_SHORT).show();
        }

//
        /*
        * set confirm code things ....
        */
    }

    @Override
    public void onBackPressed() {

        if (lConfirm.getVisibility() == View.VISIBLE) {
            lMain.setVisibility(View.VISIBLE);
            lConfirm.setVisibility(View.GONE);
        } else {

            // Toast.makeText(this, "back", Toast.LENGTH_SHORT).show();
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
            onDestroy();

        }
    }

    public void confirm(View view) {
        progress1 = ProgressDialog.show(new ContextThemeWrapper(this, R.style.progressDialog), "تأكيد",
                "يرجى الانتظار قليلاً ....", true);
        if ((txtConfirm.getText().toString().trim()).length() > 0) {

            phoneNo = txtPhoneNo.getText().toString();
            verifyPhoneNumberWithCode(LocalData.get(getApplicationContext(), "verificationId"), txtConfirm.getText().toString());
        } else {
            Toast.makeText(getBaseContext(), "يرجى ادخال رمز التاكيد", Toast.LENGTH_SHORT).show();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();
                            //        Toast.makeText(getApplicationContext(), user.getUid() + "", Toast.LENGTH_SHORT).show();
                            LocalData.add(getApplicationContext(), "phone_auth_id", String.valueOf(user.getUid()));
                            BgLoginOrSingup bgLoginOrSingup = new BgLoginOrSingup(getApplicationContext());
                            bgLoginOrSingup.execute(txtPhoneNo.toString());
                            // ...
                        } else {
                            progress1.dismiss();
                            Toast.makeText(getApplicationContext(), "خطاء في المصادقة حاول مرة اخرى", Toast.LENGTH_SHORT).show();
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(getApplicationContext(), "رمز التاكيد الذي ادخلته خاطئ يرجى التاكد", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                });
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }


    private class BgLoginOrSingup extends AsyncTask<String, Void, String> {
        Context context;
        int HttpResult;

        //ProgressBar pr;
        BgLoginOrSingup(Context ctx) {
            context = ctx;
        }

        @Override
        protected String doInBackground(String... params) {
            String login_url = PV.loginOrSignUpUrl();
            try {
                URL object = new URL(login_url);
                final HttpURLConnection con = (HttpURLConnection) object.openConnection();
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                con.setRequestProperty("Accept", "application/json");
                con.setConnectTimeout(10000);
                con.setRequestMethod("POST");

                JSONObject parent = new JSONObject();

                parent.put("PhoneNumber", txtPhoneNo.getText());


                OutputStream wr = con.getOutputStream();
                wr.write(parent.toString().getBytes("UTF-8"));
                wr.flush();
                String result = "";
                HttpResult = con.getResponseCode();

                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                    br.close();
                }
                Log.v("thugj", result + "result");

//                JSONObject main = new JSONObject(result);

                if (result != "")
                    userId = Integer.parseInt(result);

                return result;


            } catch (MalformedURLException e) {
                Log.v("thug", e.toString() + "1");
                progress1.dismiss();
                e.printStackTrace();
            } catch (IOException e) {
                Log.v("thug", e.toString() + "2");
                progress1.dismiss();


                e.printStackTrace();
            } catch (JSONException e) {
                Log.v("thug", e.toString() + "3");
                progress1.dismiss();


                e.printStackTrace();
            }
            return null;
        }

        protected void onPreExecute() {
            //   fragRegiserDriverBtnRegister.setText("تسجيل الدخول ...");

        }

        @Override
        protected void onPostExecute(String result) {
            if (HttpResult == 400) {
                Toast.makeText(context, "لايوجد مستخدم مربوط بهذا الرقم..!!!", Toast.LENGTH_LONG).show();
                progress1.dismiss();
            } else {
                progress1.dismiss();
                Intent companiesIntent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(companiesIntent);

                //companiesIntent.putExtra("userId", userId);
                LocalData.add(getApplication(), "userId", String.valueOf(userId));
            }

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }


    }


}
