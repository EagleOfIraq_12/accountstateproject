package team.morabaa.com.accountstateproject.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by eagle on 9/19/2017.
 */
public class PublicClass {
    public static String fontCiro= "fonts/CairoRegular.ttf";
    public static Typeface BoldFont(Context ctx) {
        Typeface bold = Typeface.createFromAsset(ctx.getAssets(), "fonts/CairoBold.ttf");
        return bold;
    }
    public static ArrayList<View> getAllChildren(View v) {


        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup vg = (ViewGroup) v;
        for (int i = 0; i < vg.getChildCount(); i++) {

            View child = vg.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(getAllChildren(child));

            result.addAll(viewArrayList);
        }
        return result;
    }

    //region Formatting Numbers and Prices
    private  static String _format = "#,##0.##";
    public static String ToFormattedPrice(float price){
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        DecimalFormat formatter = (DecimalFormat)nf;
        formatter.applyPattern(_format);
        return formatter.format(price * -1f) ;
    }
    //endregion
}
