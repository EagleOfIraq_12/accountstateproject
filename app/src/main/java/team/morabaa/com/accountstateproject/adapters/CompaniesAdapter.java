package team.morabaa.com.accountstateproject.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import team.morabaa.com.accountstateproject.Model.Company;
import team.morabaa.com.accountstateproject.PV;
import team.morabaa.com.accountstateproject.R;
import team.morabaa.com.accountstateproject.Utils.SQLiteConnector;

import static team.morabaa.com.accountstateproject.Model.Company.companyNameById;

/**
 * -Created by eagle on 11/2/2017.
 */

public class CompaniesAdapter extends RecyclerView.Adapter<CompaniesViewHolder> {
    FragmentTransaction fragmentTransaction;
    SwipeRefreshLayout mainSwipeRefresh;
    List<Company> companies;
    Context ctx;
    List<Company> companyImages;
    boolean isOnline;
    CompaniesViewHolder holderTask;

    public CompaniesAdapter(FragmentTransaction fragmentTransaction, SwipeRefreshLayout mainSwipeRefresh, List<Company> companies, Context ctx, List<Company> companyImages, boolean isOnline) {
        this.fragmentTransaction = fragmentTransaction;
        this.mainSwipeRefresh = mainSwipeRefresh;
        this.companies = companies;
//        this.companies = companies;
        this.ctx = ctx;
        this.companyImages = companyImages;
        this.isOnline = isOnline;
    }

    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.company_main, parent, false);
        return new CompaniesViewHolder(view);
    }

    int t = 0;

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, @SuppressLint("RecyclerView") int position) {
        //   Toast.makeText(ctx, ""+t++, Toast.LENGTH_SHORT).show();
        Typeface font = Typeface.createFromAsset(ctx.getAssets(), "fonts/CairoRegular.ttf");
        Typeface fontBold = Typeface.createFromAsset(ctx.getAssets(), "fonts/CairoBold.ttf");
        holderTask = holder;

        holder.txtCompanyName.setText(companyNameById(ctx, companies.get(position).getId()));
        holder.txtCompanyName.setTypeface(fontBold);

        holder.txtCompanyLastUpdate.setText(companies.get(position).getLastUpdate());
        holder.txtCompanyLastUpdate.setTypeface(font);


        //region set company photo
        if (!companyImages.isEmpty()) {
            if (companies.get(position).getId() == companyImages.get(position).getId()) {
                if (companyImages.get(position).getPhoto_url() == null) {
                    if (isOnline) {
                        // Toast.makeText(ctx, "image of current company does not stored in table ,you should bring it from api and update the table", Toast.LENGTH_LONG).show();
                        //get the image from api , bind it to view and update companies table
                        BgGetCompanyImaeg bgGetCompanyImaeg = new BgGetCompanyImaeg(ctx, position, companyImages.get(position).getId(), holder);
                        bgGetCompanyImaeg.execute();
                    }
                } else {
                    byte[] decodedString = Base64.decode(companyImages.get(position).getPhoto_url(), Base64.DEFAULT);
                    Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    holder.IVCompanyPhoto.setImageBitmap(bmp);
                }
            }
        } else {
            //get the image from api , bind it to view and update companies table
           BgGetCompanyImaeg bgGetCompanyImaeg = new BgGetCompanyImaeg(ctx, position, companies.get(position).getId(), holder);
            bgGetCompanyImaeg.execute();
        }


        //endregion

        holder.balancesRecyclerView.setAdapter(
                new BalancesAdapter(
                        ctx, companies.get(position).getBalances(), fragmentTransaction)
        );

        holder.balancesRecyclerView.setLayoutManager(new LinearLayoutManager(ctx));

        mainSwipeRefresh.setRefreshing(false);
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }

    //get image task
    private class BgGetCompanyImaeg extends AsyncTask<String, Void, String> {
        Context context;
        int pos;
        byte[] byteImage;
        int companyId;
        CompaniesViewHolder holder;

        public BgGetCompanyImaeg(Context context, int pos, int companyId, CompaniesViewHolder holder) {
            this.context = context;
            this.pos = pos;
            this.companyId = companyId;
            this.holder = holder;
        }

        @Override
        protected String doInBackground(String... params) {


            String UpdateUser_URL = PV.GetCompanyImagesURL();
            try {
                URL object = new URL(UpdateUser_URL);

                HttpURLConnection con = (HttpURLConnection) object.openConnection();

                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                con.setRequestProperty("Accept", "application/json");
                con.setConnectTimeout(10000);
                con.setRequestMethod("POST");

                JSONObject parent = new JSONObject();

                parent.put("Id", companyId);
                OutputStream wr = con.getOutputStream();
                wr.write(parent.toString().getBytes("UTF-8"));
                wr.flush();
                String result = "";
                int HttpResult = con.getResponseCode();

                if (HttpResult == HttpURLConnection.HTTP_OK) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                    br.close();
                }

                byteImage = result.getBytes();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            byte[] decodedString = Base64.decode(byteImage, Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            //   Toast.makeText(ctx, bmp + "", Toast.LENGTH_SHORT).show();
            Log.v("img", bmp + "");
            holder.IVCompanyPhoto.setImageBitmap(bmp);
            //next update table companies ......
            //  Company.updateCompanyImage(context, byteImage, companyId);
            new SQLiteConnector(ctx).updateCompanyImage(byteImage,companyId);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
    }

}

class CompaniesViewHolder extends RecyclerView.ViewHolder {
    LinearLayout view;
    TextView txtCompanyLastUpdate;
    TextView txtCompanyName;
    ImageView IVCompanyPhoto;
    RecyclerView balancesRecyclerView;


    CompaniesViewHolder(View view) {
        super(view);
        this.view = (LinearLayout) view;
        this.txtCompanyLastUpdate = view.findViewById(R.id.txtCompanyLastUpdate);
        this.txtCompanyName = view.findViewById(R.id.txtCompanyName);
        this.IVCompanyPhoto = view.findViewById(R.id.IVCompanyPhoto);
        this.balancesRecyclerView = view.findViewById(R.id.balancesRecyclerView);

    }
}