package team.morabaa.com.accountstateproject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import team.morabaa.com.accountstateproject.Utils.LocalData;
import team.morabaa.com.accountstateproject.Utils.PublicClass;
import team.morabaa.com.accountstateproject.Utils.SQLiteConnector;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DrawerLocker {

    int userId;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    public static ImageView toolbar_back;
    public static Button online_error_btn_refresh;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/CairoRegular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (LocalData.get(this, "userId").equals("")) {
            Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
            startActivity(intent);
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar_back = (ImageView) toolbar.findViewById(R.id.toolbar_back);

        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent myIntent = getIntent();
//        userId = myIntent.getStringExtra("userId");
//        Toast.makeText(this, userId + "", Toast.LENGTH_SHORT).show();

        online_error_btn_refresh = (Button) findViewById(R.id.online_error_btn_refresh);
        setSupportActionBar(null);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        MainFragment mainFragment = new MainFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.mainFrame, mainFragment)
                .addToBackStack(null).commit();
        setNavView();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
            onDestroy();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onStop() {
        super.onStop();
        LocalData.add(this, "isFirst", "true");
    }

    @Override
    public void setDrawerLocked(Boolean d1) {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle.setDrawerIndicatorEnabled(false);
    }

    public void logout(View view) {

        LocalData.add(getApplicationContext(), "userId", "");
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);

        startActivity(intent);
    }

    private void setNavView() {


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            View header = navigationView.getHeaderView(0);
            ImageView userImage = (ImageView) header.findViewById(R.id.image_user);
            LinearLayout btn_main = (LinearLayout) header.findViewById(R.id.btn_Main);
            LinearLayout btn_logout = (LinearLayout) header.findViewById(R.id.btn_logout);


            ((TextView) ((LinearLayout) btn_main.getChildAt(0)).getChildAt(1)).setTypeface(PublicClass.BoldFont(getApplicationContext()));
            ((TextView) ((LinearLayout) btn_logout.getChildAt(0)).getChildAt(1)).setTypeface(PublicClass.BoldFont(getApplicationContext()));
            ((LinearLayout) btn_logout.getChildAt(0)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LocalData.add(getApplicationContext(), "userId", "");
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);

                    startActivity(intent);
                    drawer.closeDrawers();
                }
            });
            ((LinearLayout) btn_main.getChildAt(0)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    MainFragment mainFragment = new MainFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.mainFrame, mainFragment)
                            .addToBackStack(null).commit();

                    drawer.closeDrawers();
                }
            });

        }
    }


}
