package team.morabaa.com.accountstateproject;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import team.morabaa.com.accountstateproject.Model.Balance;
import team.morabaa.com.accountstateproject.Model.Company;
import team.morabaa.com.accountstateproject.Model.Currency;
import team.morabaa.com.accountstateproject.Utils.LocalData;
import team.morabaa.com.accountstateproject.adapters.CompaniesAdapter;


public class MainFragment extends Fragment {

    LinearLayout layoutCompaniesContainer;
    int userId;
    static Context ctx;
    static List<Company> companies;
    List<Currency> currencies;
    static RecyclerView companiesRecyclerView;
    private static SwipeRefreshLayout mainSwipeRefresh;
    private static List<Company> companyImages;

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        companies = Company.getData(getContext());
        ctx = getContext();
        currencies = Currency.getData(getContext());
        if (!LocalData.get(getActivity(), "userId").equals("")) {
            userId = Integer.parseInt(LocalData.get(getContext(), "userId"));
        }
        fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
    }

    static FragmentTransaction fragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.toolbar_back.setVisibility(View.GONE);
        return inflater.inflate(R.layout.fragment_main, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mainSwipeRefresh = view.findViewById(R.id.mainSwipeRefresh);
        layoutCompaniesContainer = new LinearLayout(getContext()); // = view.findViewById(R.id.layoutCopaniesContainer);
        companiesRecyclerView = view.findViewById(R.id.companiesRecyclerView);
        companyImages = new ArrayList<>();
        companyImages = Company.getData(getContext());
        mainSwipeRefresh.setRefreshing(true);


        {
            // init queue of stringRequests
        }
        String isFirst = LocalData.get(getContext(), "isFirst");
        if (isFirst.equals("true")) {
            getBalances(userId);
        } else if (isFirst.equals("false")) {
            getOfflineBalances();
        }
        if (Company.getCompaniesCount(getContext()) < 1) {
            layoutCompaniesContainer.removeAllViews();
            getBalances(userId);
        }
        mainSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                layoutCompaniesContainer.removeAllViews();
                getBalances(userId);
            }
        });
        MainActivity.online_error_btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainSwipeRefresh.setRefreshing(true);
                layoutCompaniesContainer.removeAllViews();
                getBalances(userId);
            }
        });

    }

    @Override
    public void onDetach() {
        super.onDetach();
        layoutCompaniesContainer.removeAllViews();

    }

    float prevAmount;

    public static void refreshAdapter() {

        companiesRecyclerView.setAdapter(
                new CompaniesAdapter(fragmentTransaction,
                        mainSwipeRefresh, companies,
                        ctx, companyImages, true)
        );
        companiesRecyclerView.setLayoutManager(new LinearLayoutManager(ctx));


    }

    public static List<Balance> parse(String string) throws JSONException {
        List<Balance> data = new ArrayList<>();
        JSONArray mainObject = new JSONArray(string);
        for (int i = 0; i < mainObject.length(); i++) {
            Balance m = new Balance();
            JSONObject j = mainObject.getJSONObject(i);
            m.setId(j.getInt("Id"));
            m.setUserId(j.getInt("UserID"));
            m.setCompanyId(j.getInt("CompanyId"));
            m.setCurrencyId(j.getInt("CurrencyId"));
            m.setAmount(j.getInt("Amount"));
            m.setLastUpdate(j.getString("LastUpdate"));
            m.setPrevAmount(Balance.getSingleBalance(ctx,
                    j.getInt("UserID"),
                    j.getInt("CompanyId"),
                    j.getInt("CurrencyId")
            ).getAmount());
            data.add(m);
        }
        return data;
    }

    void getBalances(int id) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            String URL = PV.getBalancesURL();
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("MyUserID", id);
            final String requestBody = jsonBody.toString();
            StringRequest stringRequest =
                    new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            MainActivity.online_error_btn_refresh.setVisibility(View.GONE);
                            List<Balance> balances = new ArrayList<>();
                            try {
                                balances = parse(response);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Balance.delete(getContext(), userId);
                            for (Balance balance : balances) {
                                Balance.add(getContext(), balance);
                            }

                            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                            companies = Company.getData(balances);
                            for (Balance balance : balances) {
                                boolean exist = false;
                                for (Company company : companies) {
                                    if (company.getId() != balance.getCompanyId()) {
                                    }
                                }
                                if (!exist) {
                                    Company.getCompaniesNames(getContext(), userId,
                                            companiesRecyclerView
                                    );

                                    break;
                                }
                            }

                            for (Balance balance : balances) {
                                boolean exist = false;

                                for (Currency currency : currencies) {
                                    if (currency.getId() != balance.getCompanyId()) {
                                        exist = true;
                                    }
                                }
                                if (!exist) {
                                    Currency.getCurrenciesNames(getContext(), userId,
                                            companiesRecyclerView
                                    );
                                    break;
                                }
                            }

                            refreshAdapter();

                            mainSwipeRefresh.setRefreshing(false);

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //............
                            MainActivity.online_error_btn_refresh.setVisibility(View.VISIBLE);
                            getOfflineBalances();

                            Log.e("VOLLEY", error.toString());
                        }

                    }) {
                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }

                        @Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response) {
                            String parsed;
                            try {
                                parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                            } catch (UnsupportedEncodingException e) {
                                parsed = new String(response.data);
                            }
                            return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
                        }
                    };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {

            e.printStackTrace();
        }
    }

    private void getOfflineBalances() {
        List<Balance> balances = Balance.getData(MainFragment.this.getContext(), userId);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        companies = Company.getData(balances);
        //off line
        companiesRecyclerView.setAdapter(
                new CompaniesAdapter(fragmentTransaction,
                        mainSwipeRefresh, companies,
                        getContext(), companyImages, false)
        );
        companiesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

       /* Company.showContent(
                fragmentTransaction,
                mainSwipeRefresh,
                companies,
                layoutCompaniesContainer,
                getContext());*/
    }

}