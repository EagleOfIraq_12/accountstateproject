package team.morabaa.com.accountstateproject.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import team.morabaa.com.accountstateproject.Utils.PublicClass;
import team.morabaa.com.accountstateproject.Utils.SQLiteConnector;

/**
 * Created by eagle on 9/13/2017.
 */


public class Movement {

    private static final String tableName = "movements";
    public static String createTable =
            "CREATE TABLE `movements` (" +
                    "`id`INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    "`userId`INTEGER NOT NULL," +
                    "`companyId`INTEGER NOT NULL," +
                    "`currencyId`INTEGER NOT NULL," +
                    "`amount` REAL NOT NULL," +
                    "`type`TEXT NOT NULL," +
                    "`description`TEXT NOT NULL," +
                    "`date`TEXT NOT NULL" +
                    ");";
    //region properties
    private int id;
    private int userId;
    private int companyId;
    private int currencyId;
    private float amount;
    private int type;
    //endregion

    //region other methods
    private String description;
    private String date;

    public Movement() {

    }

    //endregion

    //region database

    public static List<Movement> parse(String JSONString, int userId) throws JSONException {
        List<Movement> data = new ArrayList<>();
        JSONArray movementsArray = new JSONArray(JSONString);
        for (int i = 0; i < movementsArray.length(); i++) {
            JSONObject movementObject = movementsArray.getJSONObject(i);
            data.add(
                    new Movement() {{
                        setId(movementObject.getInt("Id"));
                        setUserId(userId);
                        setType(movementObject.getInt("OpType"));
                        setCompanyId(movementObject.getInt("CompanyId"));
                        setCurrencyId(movementObject.getInt("CurrencyId"));
                        setAmount(movementObject.getInt("Amount"));
                        setDescription(movementObject.getString("Description"));
                        setDate(movementObject.getString("Date"));
                    }}
            );
        }
        return data;
    }

    public static String formattedDate(String rawDate) {
        String date = rawDate.substring(0, 10);
        String time = rawDate.substring(11, 16);
        int hours = Integer.parseInt(time.substring(0, 2));
        int minutes = Integer.parseInt(time.substring(3));
        String format = "AM";
        if (hours > 12) {
            hours -= 12;
            format = "PM";
        }
        return date + "\t" + hours + ":" + minutes + " " + format;
    }

    public static void add(Context ctx, Movement movement) {

        ContentValues values = new ContentValues();
        values.put("userId", movement.getUserId());
        values.put("companyId", movement.getCompanyId());
        values.put("currencyId", movement.getCurrencyId());
        values.put("amount", movement.getAmount());
        values.put("type", movement.getType());
        values.put("description", movement.getDescription());
        values.put("date", movement.getDate());
        new SQLiteConnector(ctx).add(tableName, values);
    }

    public static void delete(Context ctx, int userId, int companyId, int currencyId) {
        new SQLiteConnector(ctx).delete(tableName,
                "userId=? and companyId=? and currencyId=?",
                new String[]{String.valueOf(userId), String.valueOf(companyId), String.valueOf(currencyId)});
    }

    public static List<Movement> getData(Context ctx, int userId, int companyId, int currencyId) {

        List<Movement> movements = new ArrayList<>();
        Cursor cursor = new SQLiteConnector(ctx).getData(tableName);
        Log.e("VOLLEY", "--------\tcursor Count\t-----------" + cursor.getCount());
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            if (cursor.getInt(1) == userId &&
                    cursor.getInt(2) == companyId &&
                    cursor.getInt(3) == currencyId) {
                Log.e("VOLLEY",
                        cursor.getInt(1) + "\n" +
                                cursor.getInt(2) + "\n" +
                                cursor.getInt(3)
                );
                movements.add(
                        new Movement() {{
                            setId(cursor.getInt(0));
                            setUserId(cursor.getInt(1));
                            setCompanyId(cursor.getInt(2));
                            setCurrencyId(cursor.getInt(3));
                            setAmount(cursor.getInt(4));
                            setType(cursor.getInt(5));
                            setDescription(cursor.getString(6));
                            setDate(cursor.getString(7));
                        }}
                );
            }
            cursor.moveToNext();
        }
        Log.e("VOLLEY", "----------------\t" + movements.size());

        return movements;
    }
    //endregion

    public static float getSum(Context ctx, int userId, int companyId, int currencyId) {

        float sum = 0;
        Cursor cursor = new SQLiteConnector(ctx).getData(tableName);
        Log.e("VOLLEY", "--------\tcursor Count\t-----------" + cursor.getCount());
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (cursor.getInt(1) == userId &&
                    cursor.getInt(2) == companyId &&
                    cursor.getInt(3) == currencyId) {
                sum += cursor.getFloat(4);
            }
            cursor.moveToNext();
        }
        return sum;
    }

    //region getters & setters
    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getStringAmount() {
        return PublicClass.ToFormattedPrice(amount);
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    //endregion

}