package team.morabaa.com.accountstateproject;

/**
 * Created by thulfkcar on 12/6/2017.
 */

public class PV {
    private static String Domain = "https://mort.morabaa.com/api/";
    private static String loginOrSignUp = Domain + "MyUser/GetUsingPhoneNum";
    private static String getBalances = Domain + "balance/GetBalance";
    private static String getCompanies = Domain + "Company/GetCompanies";
    private static String getCurrencies = Domain + "currency/GetCurrencies";
    private static String getMovements = Domain + "Movement/GetBalanceDetails";
    private static String GetCompanyImage = Domain + "Company/getCompanyImage";

    public static String loginOrSignUpUrl() {
        return loginOrSignUp;
    }

    public static String getBalancesURL() {
        return getBalances;
    }

    public static String getCompaniesURL() {
        return getCompanies;
    }

    public static String getCurrenciesURL() {
        return getCurrencies;
    }

    public static String getMovementsURL() {
        return getMovements;
    }

    public static String GetCompanyImagesURL() {
        return GetCompanyImage;
    }
}


//userId