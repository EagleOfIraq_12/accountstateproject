package team.morabaa.com.accountstateproject.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import team.morabaa.com.accountstateproject.Model.Balance;
import team.morabaa.com.accountstateproject.Model.Currency;
import team.morabaa.com.accountstateproject.MovementsFragment;
import team.morabaa.com.accountstateproject.R;

/**
 * -Created by eagle on 11/2/2017.
 */

public class BalancesAdapter extends RecyclerView.Adapter<BalancesViewHolder> {
    private List<Balance> balances;
    private Context ctx;
    private FragmentTransaction fragmentTransaction;

    public BalancesAdapter(Context ctx, List<Balance> balances, FragmentTransaction fragmentTransaction) {
        this.balances = balances;
        this.ctx = ctx;
        this.fragmentTransaction = fragmentTransaction;
    }

    @Override
    public BalancesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.balance_layout, parent, false);
        return new BalancesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BalancesViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Typeface font = Typeface.createFromAsset(ctx.getAssets(), "fonts/CairoRegular.ttf");

        //  ImageView imageViewIcon = vi.findViewById(R.id.imageViewIcon);


        holder.tXtAmount.setText(balances.get(position).getStringAmount() + "");
        holder.tXtAmount.setTypeface(font);


        holder.tXtCurrency.setText(Currency.getSympleById(ctx, balances.get(position).getCurrencyId()));
        holder.tXtCurrency.setTypeface(font);

        if (balances.get(position).getAmount() < 0) {
            //  imageViewIcon.setImageResource(R.drawable.moneyred_icon);

            holder.tXtAmount.setTextColor(Color.parseColor("#00E676"));

        } else {
            holder.tXtAmount.setTextColor(Color.parseColor("#ff8a80"));
            //    holder.tXtCurrency.setTextColor(Color.parseColor("#00E676"));
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("userId", balances.get(position).getUserId());
                bundle.putInt("companyId", balances.get(position).getCompanyId());
                bundle.putInt("currencyId", balances.get(position).getCurrencyId());
                TextView txtCompanyName =
                        ((View) holder.view.getParent().getParent())
                                .findViewById(R.id.txtCompanyName);
                bundle.putInt("companyId", balances.get(position).getCompanyId());
                bundle.putString("companyName", txtCompanyName.getText().toString());
                bundle.putString("currencyName", holder.tXtCurrency.getText().toString());
                bundle.putString("Amount", holder.tXtAmount.getText().toString());

                bundle.putFloat("PrevAmount",balances.get(position).getPrevAmount());
                bundle.putFloat("FloatAmount", balances.get(position).getAmount());
                bundle.putInt("balanceId", balances.get(position).getId());
                MovementsFragment movementsFragment = new MovementsFragment();
                movementsFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.mainFrame, movementsFragment).addToBackStack(null).commit();
            }
        });


    }

    @Override
    public int getItemCount() {
        return balances.size();
    }


}

class BalancesViewHolder extends RecyclerView.ViewHolder {
    LinearLayout view;
    TextView tXtAmount;
    TextView tXtCurrency;


    BalancesViewHolder(View view) {
        super(view);
        this.view = (LinearLayout) view;
        this.tXtAmount = view.findViewById(R.id.tXtAmount);
        this.tXtCurrency = view.findViewById(R.id.tXtCurrency);
    }
}